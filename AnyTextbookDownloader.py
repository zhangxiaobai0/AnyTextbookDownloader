import os
import requests
from bs4 import BeautifulSoup
import tkinter as tk
from tkinter import filedialog, messagebox, ttk
import threading
import re
from datetime import datetime
import json

#20240302 修复中小学云平台无法下载部分私域课本，即人教版/部编版的部分课本，如果还有问题请及时指出

# 初始化下载目录为默认目录，如果使用Linux系统需要自行重新设置目录
download_directory = r'C:\TextbookDownloads'

def browse_directory():
    global download_directory
    # 弹出文件选择框让用户选择保存目录
    folder_path = filedialog.askdirectory()
    # 如果用户选择了一个新的目录，那么更新下载目录
    if folder_path != "":
        download_directory = folder_path
    # 清空文本框并插入新的下载目录
    directory_entry.delete(0, tk.END)
    directory_entry.insert(0, download_directory)

def download_book():
    # 获取用户输入的URL
    book_url = url_entry.get()
    if "basic.smartedu.cn" in book_url:  # 判断是否为第二种下载链接
        download_smartedu_content(book_url)
    else:
        download_pep_content(book_url)

def download_smartedu_content(url):
    content_id_match = re.search(r'contentId=([^&]+)', url)
    if content_id_match:
        content_id = content_id_match.group(1)
        title_url = f"https://s-file-2.ykt.cbern.com.cn/zxx/ndrv2/resources/tch_material/details/{content_id}.json"
        headers={
        'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0'
    }
        response = requests.get(url=title_url,headers=headers)
        json_data = response.text
        title_match = re.search(r'"title"\s*:\s*"([^"]+)"', json_data)
        if title_match:
            title_tag = title_match.group(1)
            if title_tag:
                title = title_tag.strip()  # 获取网页标题并去除两端空格
            else:
                title = None
            if title:
                    sub_directory = title + " " + datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
            else:
                sub_directory = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')

            valid_sub_directory = "".join(c if c.isalnum() or c.isspace() else "_" for c in sub_directory)
            full_path = os.path.join(download_directory, valid_sub_directory)
            os.makedirs(full_path, exist_ok=True)
            download_url = f"https://r2-ndr.ykt.cbern.com.cn/edu_product/esp/assets/{content_id}.pkg/pdf.pdf"
            download_file(download_url, full_path, valid_sub_directory, title)
        else:
            messagebox.showwarning("Warning", "30000")
    
def download_pep_content(url):
    response = requests.get(url)
    response.encoding = response.apparent_encoding
    soup = BeautifulSoup(response.text, 'lxml')
    #创建文件
    title_tag = soup.title
    if title_tag:
        title = title_tag.string.strip()  # 获取网页标题并去除两端空格
    else:
        title = "Untitled"

    valid_title = "".join(c if c.isalnum() or c.isspace() else "_" for c in title)
    full_path = os.path.join(download_directory, valid_title)
    os.makedirs(full_path, exist_ok=True)

    #提取id
    pattern = r"/(\d+)/mobile/index\.html"
    match = re.search(pattern, url)
    if match:
        book_id = match.group(1)
        js_url = "https://book.pep.com.cn/"+book_id+"/mobile/javascript/config.js"
        #提取页面
        js_response = requests.get(js_url)
        if js_response.status_code == 200:
            js_content = js_response.text
            pattern = r"bookConfig\.totalPageCount\s*=\s*(\d+);"
            match = re.search(pattern, js_content)
            if match:
                page_count = match.group(1)
                if int(page_count) > 0:
                    progress_window = tk.Toplevel(root)
                    progress_window.title("下载进度")
                    progress_label = tk.Label(progress_window, text="正在下载...")
                    progress_label.pack(padx=20, pady=10)
                    progress_bar = ttk.Progressbar(progress_window, length=300, mode="determinate", maximum=int(page_count))
                    progress_bar.pack(padx=20, pady=10)
                    def download_pages():
                        for page_number in range(1, int(page_count) + 1):
                            page_url = f"https://book.pep.com.cn/{book_id}/files/mobile/{page_number}.jpg"
                            response = requests.get(page_url)
                            if response.status_code == 200:
                                file_path = os.path.join(full_path, f"{page_number}.jpg")
                                with open(file_path, 'wb') as file:
                                    file.write(response.content)
                                progress_bar["value"] = page_number
                                progress_label.config(text=f"正在下载：{page_number}/{int(page_count)}")
                                progress_window.update()
                                # 添加下载完成的提示
                                if page_number == int(page_count):
                                    progress_label.config(text="下载完成")
                                    messagebox.showinfo("下载完成", f"《{title}》 图片下载完成")
                                    progress_window.destroy()
                    threading.Thread(target=download_pages).start() 
                else:
                    messagebox.showwarning("Warning", "40000")
            else:
                messagebox.showwarning("Warning", "40001")
        else:
            messagebox.showwarning("Warning", "40002")
    else:
        messagebox.showwarning("Warning", "40003")

def download_file(url, directory, filename, title):
    response = requests.get(url)
    if response.status_code == 200:
        file_path = os.path.join(directory, f"{filename}.pdf")
        with open(file_path, 'wb') as file:
            file.write(response.content)
        messagebox.showinfo("下载完成", f"{title}.pdf 下载完成")
    else:
        messagebox.showerror("下载失败", f"{title}.pdf 下载失败")


'''
#单独的放置列
root = tk.Tk()
root.title("电子教材下载器")
root.geometry("460x200")
root.resizable(False, False)

url_label = tk.Label(root, text="请输入电子教材地址：")
url_label.grid(row=0, column=0, sticky='w', padx=5, pady=5)

url_entry = tk.Entry(root, width=50)
url_entry.grid(row=1, column=0, sticky='ew', padx=5, pady=5)

directory_label = tk.Label(root, text="请选择下载目录：")
directory_label.grid(row=2, column=0, sticky='w', padx=5, pady=5)

directory_entry = tk.Entry(root, width=50)
directory_entry.insert(0, download_directory)
directory_entry.grid(row=3, column=0, sticky='ew', padx=5, pady=5)

browse_button = tk.Button(root, text="浏览", command=browse_directory)
browse_button.grid(row=4, column=1, sticky='ew', padx=5, pady=5)

download_button = tk.Button(root, text="下载", command=download_book)
download_button.grid(row=4, column=2, sticky='ew', padx=5, pady=5)
'''

#固定宽度的
root = tk.Tk()
root.title("电子教材下载器")
root.geometry("460x200") 
root.resizable(False, False)
label = tk.Label(root, text="")
label.grid(row=0, column=0, sticky='w', padx=5, pady=5)
url_label = tk.Label(root, text="请输入电子教材地址：支持人教社电子教材下载（图），中小学云平台下载（PDF）")
url_label.grid(row=0, column=0, sticky='w', padx=5, pady=5)

url_entry = tk.Entry(root, width=55)
url_entry.grid(row=1, column=0, sticky='ew', padx=5, pady=5)

directory_label = tk.Label(root, text="请选择下载目录：")
directory_label.grid(row=2, column=0, sticky='w', padx=5, pady=5)

directory_entry = tk.Entry(root, width=55)
directory_entry.insert(0, download_directory)
directory_entry.grid(row=3, column=0, sticky='ew', padx=5, pady=5)

button_frame = tk.Frame(root)
#左边的
button_frame.grid(row=4, column=0, sticky='w')
#右边的
# button_frame.grid(row=4, column=0, sticky='e')
#中间的
#button_frame.grid(row=4, column=0)

browse_button = tk.Button(button_frame, text="浏览", command=browse_directory, width=10)
browse_button.pack(side='left', padx=5, pady=5)

download_button = tk.Button(button_frame, text="下载", command=download_book, width=10)
download_button.pack(side='left', padx=5, pady=5)


root.mainloop()